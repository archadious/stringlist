
//-----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include "str_list.h"

//-----------------------------------------------------------------------------
void output( const char* string ) {

  printf( "DB: string = '%s'\n", string );

};  // end output


void print_list( str_list_t* list ) {

  str_list_foreach( list, output );

};  // end print_list

//-----------------------------------------------------------------------------
int main( int argc, char* argv[] ) {

  str_list_t list;
  str_list_t blist;

  str_list_init( &list );
  str_list_init( &blist );

  str_list_add( &list, "one" );
  str_list_add( &list, "two" );
  str_list_add( &list, "three" );

  str_list_add_sep( &blist, "four:five:six:seven:eight", ':' );



  print_list( &list );
  printf( "DB: size of list is %d\n\n", str_list_count( &list ) );


  print_list( &blist );
  printf( "DB: size of blist is %d\n\n", str_list_count( &blist ) );

  str_list_append( &list, &blist );
  printf( "DB: size of list is now %d\n\n", str_list_count( &list ) );

  printf( "DB: list = '%s'\n", str_list_stringify( &list, ' ' ) );

  str_list_free( &list );

  return EXIT_SUCCESS;

};  // end main
