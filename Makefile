
CC := /usr/bin/gcc
CFLAGS := -g --std=c17

target := main
objs := main.o str_list.a

all : $(target)
$(target) : $(objs) ; $(CC) -o $@ $^

str_list.a : str_list.o ; ar -csr $@ $<
str_list.o : str_list.c str_list.h ; $(CC) $(CFLAGS) -c -o $@ $<

.PHONY : clean
clean : ; rm --force *.o *.a $(target)
