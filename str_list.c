// Time-stamp: <2021-03-18 11:13:57 daniel>


//
// str_list.c
//


// Required header files
//-----------------------------------------------------------------------------
#include <stdlib.h>  // for 'free'
#include <argz.h>  // accumlative command line strings
#include "str_list.h"


//
// NOTE:  Should check for NULL being passed to each function
//


// @name str_list_init
// @brief Initialize a str_list
// @param str_list Pointer to a str_list_t
void str_list_init( str_list_t* slist ) {

  slist->string = 0x0;  // passing a str_list with data will cause mem leak
  slist->length = 0;

};  // end init_str_list


// @name str_list_isempty
// @brief Return true if str_list is empty, false if str_list has strings
// @param str_list Pointer to a str_list_t
// @retval bool true if str_list is empty or false if str_list has strings
bool str_list_isempty( str_list_t* slist ) {

  return ( slist->length == 0 );

};  // end str_list_isempty

// @name str_list_add
// @brief Add a string to a given str_list
// @param str_list Pointer to a str_list_t
// @param string The string to add to the str_list
error_t str_list_add( str_list_t* slist, const char* string ) {

  slist->error = argz_add( &slist->string, &slist->length, string );
  return slist->error;

};  // end str_list_add


// @name str_list_add_sep
// @brief Split a string by a delimiter and
// each seperate substring to the list
// @param str_list Pointer to a str_list_t
// @param string The string to be split and added
// @param delim The delimiter to split the string on
error_t str_list_add_sep( str_list_t* slist, const char* string, int delim ) {

  slist->error = argz_add_sep( &slist->string, &slist->length, string, delim );
  return slist->error;

};  // end str_list_add


// @name str_list_append
// @brief Append a string list to another string list.  Does not alter
// the list being appended.
// @param slist The string list to append to
// @param other The list to be appended
//-----------------------------------------------------------------------------
error_t str_list_append( str_list_t* slist, str_list_t* other ) {

  slist->error = argz_append(
                             &slist->string,
                             &slist->length,
                             other->string,
                             other->length
                             );
  return slist->error;


};  // end str_list_append


// @name str_list_next
// @brief Returns the next string from the str_list
// @param str_list Pointer to a str_list_t
// @param entry Current string from str_list in iteration
// @retval char* next string after entry
char* str_list_next( str_list_t* slist, const char* entry ) {

  return argz_next( slist->string, slist->length, entry );

};  // end str_list_next


// @name str_list_free
// @brief Free memory used to store the strings in this str_list
// @param str_list Pointer to a str_list_t
//-----------------------------------------------------------------------------
void str_list_free( str_list_t* slist ) {

  free( slist->string );

};  // end str_list_free


// @name str_list_count
// @brief Returns the number of strings in this list
// @param str_list_t pointer to the string list object
//-----------------------------------------------------------------------------
size_t str_list_count( str_list_t* slist ) {

  return argz_count( slist->string, slist->length );

};  // end str_list_count


// @name str_list_stringify
// @brief Transforms the list into a normal string by replacing
// all of the null bytes ('\0') except the last by a seperator.
// Note that the returned pointer must still be freed when no
// longer being used.
// @param slist Pointer to a str_list_t object
// @param sep The value to replace null with
// @retval A pointer to the modified string
//-----------------------------------------------------------------------------
char* str_list_stringify( str_list_t* slist, int sep ) {

  argz_stringify( slist->string, slist->length, sep );
  return slist->string;

};  // end str_list_stringify


//-----------------------------------------------------------------------------
void str_list_foreach( str_list_t* slist, void( *func )( const char* ) ) {

  char* string = NULL;
  const char* entry = NULL;

  while ( string = str_list_next( slist, entry ) ) {
    func( string );
    entry = string;  // advance pointer to current string in str_list
  };  // end while string

};  // end str_list_foreach


//-----------------------------------------------------------------------------
void str_list_foreach_param(
                            str_list_t* slist,
                            void( *func )( const char*, void* ),
                            void* param ) {

  char* string = NULL;
  const char* entry = NULL;

  while ( string = str_list_next( slist, entry ) ) {
    func( string, param );
    entry = string;  // advance pointer to current string in str_list
  };  // end while string

};  // end str_list_foreach
